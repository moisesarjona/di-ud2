package org.openjfx.Actividad1;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;


public class Ajedrez extends Application implements EventHandler<MouseEvent> {

    public final static int WIDTH = 50;

    private Label indicatorLabel;

    @Override
    public void start(Stage stage) throws Exception {

        stage.setTitle("Actividad 3");
        Group board = new Group();

        for (int x = 0; x < 10 ; x++) {
            for (int y = 0; y < 10 ; y++) {
                Rectangle rectangle = new Rectangle(WIDTH,WIDTH);
                rectangle.setX(x * WIDTH);
                rectangle.setY(y * WIDTH);
                rectangle.setId("Columna: " + x + " +  Fila: " + y);

                if ((x+y) % 2 == 0){
                    rectangle.setFill(javafx.scene.paint.Color.rgb(128, 64, 0));
                } else {
                    rectangle.setFill(Color.rgb(230, 200, 150));
                }
                rectangle.setOnMouseClicked(this);
                board.getChildren().add(rectangle);
            }
        }
        indicatorLabel = new Label("Nada presionnado");
        indicatorLabel.setPadding(new Insets(20,20,20,20));
        VBox layout = new VBox();
        layout.setAlignment(Pos.TOP_CENTER);
        layout.getChildren().addAll(board,indicatorLabel);

        Scene theScene = new Scene(layout,800,550);
        stage.setScene(theScene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
    @Override
    public void handle(MouseEvent mouseEvent) {

        Rectangle rectangle = (Rectangle) mouseEvent.getSource();
        indicatorLabel.setText(rectangle.getId());

    }
}
