package org.openjfx.Actividad1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Piramide extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Piramide");
        Pane root = new Pane();

        for (int i = 1; i <9; i++) {
            for (int j = 1; j < i; j++) {
                Circle circle1 = createCircle();
                circle1.setCenterX(440 - j*45);
                circle1.setCenterY(i*45);
                root.getChildren().add(circle1);

                Circle circle = createCircle();
                circle.setCenterX(350 + j*45);
                circle.setCenterY(i*45);
                root.getChildren().add(circle);
            }
        }

        Scene scene = new Scene(root,800,500);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    public static Circle createCircle (){
        Circle circle = new Circle(20);
        circle.setFill(Color.ROYALBLUE);
        circle.setStrokeWidth(3);
        circle.setStroke(Color.FUCHSIA);

        return circle;
    }
}

/*Circle circle = createCircle();
            circle.setCenterX(400 + i*30);
            circle.setCenterY(i*35);
            root.getChildren().add(circle);*/